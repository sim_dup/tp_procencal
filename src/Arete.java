public class Arete {
    private int id;
    private Point p1;
    private Point p2;

    public Arete(Point _p1, Point _p2){
        p1 = _p1;
        p2 = _p2;
    }


    public Point pass(Point p){
        if (p.getId() == p1.getId()){
            return p2;
        }
        else {
            return p1;
        }
    }

    public Point parcourir(Point pCoteA){
        //On lui donne un des points lui appartenant, elle donne l'id du point à l'autre extrémité
        this.print();
        if (pCoteA==this.p1){
            return p2;
        }
        else {
            return p1;
        }
    }

    public void print(){
        System.out.println(this.p1.getIndice()+"--"+this.p2.getIndice());
    }
}
