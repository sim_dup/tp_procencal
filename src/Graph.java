import java.util.ArrayList;
import java.io.File;

public class Graph {
    ArrayList<Point> lPoints = new ArrayList();
    ArrayList<Arete> lAretes = new ArrayList();

    public Graph() {

    }

    public Point getPointById(String  key){
        for(int i = 0; i < lPoints.size();i++){
            if (key.equals(lPoints.get(i).getId())){
                return lPoints.get(i);
            }
        }
        return null;
    }

    public Point getPointByIndice(int key){
        for(int i = 0; i < lPoints.size();i++){
            if (key==lPoints.get(i).getIndice()){
                return lPoints.get(i);
            }
        }
        return null;
    }

    public void afficher(){
        System.out.println("AFFICHAGE GRAPH :");
        System.out.println("Points : "+lPoints);
        System.out.println("Aretes : "+lAretes);
    }

    public void razVisit(){
        for (Point p:lPoints){
            p.setVisit(false);
        }
    }

    public boolean nbAreteValide(){ // renvoi false si moins de 2 arêtes
        int nbA;
        for (Point p:lPoints){
            nbA=p.nbArete();
            if (nbA<2){
                return false;
            }

        }
        return true;
    }

}

