import java.util.ArrayList;

public class Point {

    private int indice;
    private String id;
    private boolean visited;
    private int indAreteVisite;



    private ArrayList<Arete> lAreteLiee = new ArrayList();

    public Point(String _id,int indice){
        this.id = _id;
        this.indice = indice;
        this.indAreteVisite=0;

        visited = false;
    }


    public String getId() {
        return this.id;
    }

    public void setVisit(boolean _visited){
        this.visited = _visited;
    }

    public void ajoutArete(Arete newArete){
        lAreteLiee.add(newArete);
    }

    public Arete ajoutVoisin(Point newVoisin){
        Arete lien = new Arete(newVoisin,this);
        return lien;
    }

    public Point chercheVoisin(int ind){
        Point voisin;
        System.out.println("voisin cherché : "+ind);
        for (Arete a:lAreteLiee){
            voisin=a.parcourir(this);
            if(voisin.getIndice()==ind){
                return voisin;
            }


        }

        return null;
    }


    public int getIndice() {
        return indice;
    }


    public boolean isVisited() {
        return visited;
    }

    public void afficherCorrespondance(){
        System.out.println(this.id +"="+this.indice);
    }

    public int nbArete() {
        return lAreteLiee.size();
    }


    public ArrayList<Arete> getlAreteLiee() {
        return lAreteLiee;
    }

    public Point parcoursGraph(){
        if (indAreteVisite<lAreteLiee.size()) {
            Arete a = lAreteLiee.get(indAreteVisite);
            return a.parcourir(this);
        }
        else{
            return null;
        }



    }
}
