/**
 * Created by drey on 14/11/16.
 */
public class Bot {
    Point position;
    Graph g;
    Point tabPoint[];
    int nbAvance;//nombre d'avancé dans le graph


    public Bot(Graph g){
        this.g = g;
    }

    public boolean testNaif(int chemin[]){
        int i=1;
        boolean valable = true;
        position=g.getPointByIndice(chemin[0]);
        while (i<chemin.length && valable){
            valable=avancer(position,chemin[i]); //fait avancer le bot de une position. Renvoi false si le trajet est impossible (déjà visité ou pas d'arete).
            i++;

        }
        if(valable){
            System.out.println("RETOUR DEPART");
            valable=avancer(position,chemin[0]);
        }


        return valable;
    }

    public boolean avancer(Point here,int indDest){
        Point destination;
        destination=here.chercheVoisin(indDest);
        if(destination==null){
            System.out.println("Faux au point (Pas d'arete) :"+here.getIndice());
            here.afficherCorrespondance();
            return false;
        }

        if(destination.isVisited()){
            System.out.println("Faux au point (déjà visité) :"+here.getIndice());

            return false;
        }
        this.position=destination;
        this.position.setVisit(true);
        System.out.println("Déplacement sur "+position.getIndice());
        return true;
    }


    public boolean testParcours(Point depart){
        this.nbAvance=0;
        int nbDeplacements = 0;
        boolean solutionTrouve;
        this.position=depart;
        this.tabPoint[nbAvance]=depart;
        Point tPointChemin[]= new Point[g.lPoints.size()];
        tPointChemin[0]=depart;
        solutionTrouve = avancerParcours(tPointChemin,nbDeplacements);


        return true;//a enlever
    }

    private boolean avancerParcours(Point [] tChemin,int nbDeplacements) {
        if(nbDeplacements==tChemin.length){
            return true;
        }

        Point destination = tChemin[nbDeplacements].parcoursGraph();
        if(destination==null){
            if (nbDeplacements<1){ // si on essye de revenir avant le père, c'est que le cycle n'existe pas
                System.out.println("Pas de cycle hamiltonnien en commencant par le point "+tChemin[0]);
                return false;
            }

            avancerParcours(tChemin, nbDeplacements-1); //si toutes les aretes ont été parcouru, on retourne de 1 en arrière.

        }
        else{
            // voir comment faire pour que soit donné toutes les aretes adj !

            tChemin[nbDeplacements+1]=destination;




        }

    return false;//a enlever

    }


}
