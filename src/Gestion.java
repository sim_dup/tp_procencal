public class Gestion {
    public Graph g;
    boolean nonHamiltonien;


    public Gestion(){
        nonHamiltonien=false;

    }

    public void traitementImport(String fichierComplet, int nbLignes) {
        String retourLigne = System.getProperty("line.separator");
        System.out.println("Nb de lignes = " + nbLignes);
        String[] ligne = fichierComplet.split(retourLigne);
        System.out.println("Deuxieme ligne = " + ligne[2]);
        this.g = new Graph();
        Point p1;
        Point p2;
        int k=1;
        for (int i = 0; i < ligne.length; i++) {
            if (i == 2) {
                String[] tabSommets = ligne[i].split(" ");
                for (int j = 0; j < tabSommets.length; j++) {
                    Point pa = new Point(tabSommets[j],k);
                    g.lPoints.add(pa);
                    k++;

                }
            } else if (i != 0 && i != 1 && i != 2 && i != (ligne.length - 1)) {
                String stringLigne = ligne[i];
                String[] tabAretes = stringLigne.split("--");
                p1 = g.getPointById(tabAretes[0]);
                p2 = g.getPointById(tabAretes[1]);
                Arete arete = new Arete(p1, p2);
                p1.ajoutArete(arete);
                p2.ajoutArete(arete);
                g.lAretes.add(arete);
            }
        }

        g.afficher();

        // On crée un tableau tabPermutations contenant toutes les permutations possibles
        // Pour le remplir, on crée deux tableaux tabRef et tabModif qui nous permettent d'actualiser les valeurs
        // pour tabPermutations
        int nbPoints = g.lPoints.size();
        int[] tabPermutations = new int[nbPoints];

        for (int i = 0; i < nbPoints; i++) {
            tabPermutations[i] = i + 1;
        }
        permut(tabPermutations, 0);
    }

    public void afficheTab(int[] tableau, int nbPoints){

        for (int i = 0; i < tableau.length; i++) {
            System.out.print(tableau[i] + " ");
            if ((i + 1 ) % nbPoints == 0) {
                System.out.println();
            }
        }
    }

    public static final void swap (int[] a, int i, int j) {
        int t = a[i];
        a[i] = a[j];
        a[j] = t;
    }

    public void permut(int[] tab, int index){
        if (index < tab.length){
            for (int j = index; j < tab.length; j++){
                swap(tab, index, j);
                permut(tab, index + 1);
                swap(tab, index, j);
            }
        }
        else {
            afficheTab(tab, tab.length);
            // on appelle la fonction qui teste à la place
            fonctionTest(g,tab);

        }
    }

    public boolean fonctionTest(Graph g, int tab[]){
        boolean hamiltonien;

        Bot naif=new Bot(g);
        hamiltonien=naif.testNaif(tab);

        if (hamiltonien){

            this.nonHamiltonien=false;
            System.out.println("Un cycle Hamiltonien à été trouve : ");
            afficheTab(tab,tab.length);
            System.exit(0);
        }

        g.razVisit();
        return hamiltonien;
    }

}
