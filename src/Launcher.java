import java.io.*;
import java.util.ArrayList;

public class Launcher {

    public static void main(String [] args) throws Exception {
        String thisLine;
        Gestion gest = new Gestion();
        String fichierComplet = "";
        int nbLignes = 0;
        for (int i = 0; i < args.length; i++) {

            try {
                BufferedReader br = new BufferedReader(new FileReader(args[i]));
                while ((thisLine = br.readLine()) != null) {
                    System.out.println(thisLine);
                    fichierComplet = fichierComplet + "\n" + thisLine;
                    nbLignes++;
                }
            }
            catch (IOException e) {
                System.err.println("Error: " + e);
            }
        }
        gest.traitementImport(fichierComplet, nbLignes);
    }


}
